# Terrain Tilemap

Godot Plugin for Procedural Heightmap-based Open World using Tilemaps


## About

This project is based on the already existing [Heightmap Terrain](https://github.com/Zylann/godot_heightmap_plugin) plugin by Zylann which is extremely well written. Please take the time to explore this plugin if you have not already done so. 

## How To Use

Simply copy the contents of the `addons/` folder to your project folder. In particular the folder `addons/luziffer.htilemap/`. Then activate the plugin in Godot's Project Settings.

### Editor (Offline Mode) 

The plugin provides a node called `HTilemap` which is a node that models an open world made of tiles, each containing a heightmap. The tiles are stored in the **tile directory**. Simply create a new empty directory. It will be populated automatically, don't worry about it. 

If change the **tile index** the navigator lets you explore the tiles while using the editor. 

![navigator-screenshot](docs/navigator.png)

### Game (Online Mode)

When playing the game the **trigger node** will cause automatic scrolling. This means if the player comes too close to the border of a tile a new tile will be loaded and the distant tiles will be closed. Also the trigger node will be kept in the center. This avoides floating point errors if coordinates become too high.   



