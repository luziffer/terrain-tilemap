extends Spatial


func _on_tilemap_moved(vector):
	var trafo = get_node("Player/Body/CameraTarget").get_global_transform()
	var target = get_node("Player/Local/Target")
	var camera = get_node("Player/Local/Camera")
	target.set_global_transform(trafo)
	trafo = camera.get_global_transform()
	trafo.origin -= vector
	camera.set_global_transform(trafo)
	camera.look_at(target.get_global_transform().origin, Vector3(0,1,0))
	
	
