extends Panel


func _process(_delta):
	
	# Calculate values
	var player = get_node("../Player")
	var tilemap = get_node("../HTilemap")
	var p = player.get_node("Body").get_global_transform().origin
	var i = tilemap.tile_i
	var j = tilemap.tile_j
	var tiles = "\n"
	for c in tilemap.get_children():
		var x = c.translation.x
		var y = - c.translation.z
		tiles += " . %5s" % c.name + "\t: %8s \t %8s" % [String(x),String(y)] +  "\n"
	
	# Store info 
	var info = {
		"Frame rate (fps)" : Performance.get_monitor(0),
		"Player 2d Pos" : "%.2f %.2f" % [p.x, -p.z],
		"Current tile" : String(i)+"_" + String(j),
		"Loaded tiles" : tiles,
	}
	
	# Update text
	var msg = "Debug:\n\n"
	var maxlen = 0
	for label in info: 
		maxlen = max(maxlen, len(label))
	for label in info:
		msg += label + " ".repeat(maxlen - len(label)) + " : " + String(info[label]) + "\n"
	get_node("Items/Label").set_text(msg)
