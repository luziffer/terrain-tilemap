extends Spatial

export(float,0,100) var _movement_speed = 10.0
export(float,1,10) var _gravity = 4.5
export(float,0.1,1.0) var _jump_strength = 1
export(int,1,5) var _max_jumps = 2
export(float, 0,1) var _friction =  .99


var up = Vector3(0,1,0)
var velocity = Vector3()
var jumps = 0

#func _ready():
#	var view = $Local/Camera/Viewport.get_texture()
#	$Minimap.material.set_shader_param("view", view)


func _process(delta):
	
	# Read controls
	var movement = Vector2()
	movement.x += Input.get_joy_axis(0,0)
	movement.y -= Input.get_joy_axis(0,1)
	if Input.is_action_pressed("ui_left"):
		movement.x -= 1
	if Input.is_action_pressed("ui_right"):
		movement.x += 1
	if Input.is_action_pressed("ui_up"):
		movement.y += 1
	if Input.is_action_pressed("ui_down"):
		movement.y -= 1
	
	# Move body
	var basis = $Local/Camera.get_camera_transform().basis
	var forward = -basis.z
	forward.y = 0
	forward = forward.normalized()
	var sideward = basis.x
	sideward.y = 0 
	sideward = sideward.normalized()
	$Body.rotate_y(-delta * movement.x) 
	movement = movement.y * forward + movement.x/2 * sideward
	movement *= _movement_speed
	$Body.move_and_slide(movement, up)
	
	# Jump
	if $Body.is_on_floor() or $Body.is_on_wall():
		jumps = 0
	if jumps < _max_jumps:
		if Input.is_action_just_pressed("ui_accept"):
			velocity.y += _jump_strength
			jumps += 1
	
	# Gravity
	if not $Body.is_on_floor() and not $Body.is_on_wall():
		velocity.y -= delta * _gravity 
	
	# Update 
	var remainder = delta*delta*$Body.move_and_slide(velocity/delta, up)
	velocity = (1-_friction) * remainder + _friction * velocity
	var slide_count = $Body.get_slide_count()
	if slide_count > 0:
		velocity.y = 0
		jumps = 0
		
	#var collision = $Body.test_move(velocity)
	#if collision:
	#	velocity.y = 0
	
	# Adjust camera
	var trafo = $Body/CameraTarget.get_global_transform()
	$Local/Target.set_global_transform(trafo)
	var bird = $Bird/Camera.get_global_transform()
	var offset = $Body.get_global_transform().origin - trafo.origin
	bird.origin.x = trafo.origin.x + 2*offset.x
	bird.origin.z = trafo.origin.z + 2*offset.z
	$Bird/Camera.set_global_transform(bird)
	$Local/Camera.look_at($Body.get_global_transform().origin, Vector3(0,1,0))
