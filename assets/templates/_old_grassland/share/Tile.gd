extends Spatial
export(OpenSimplexNoise) var _noise

func _sample(i,j):
	var terrain = get_node("Terrain")
	var data = terrain.get_data()
	var resolution = data.get_resolution()
	
	# Get maps
	var height_map = data.get_image(data.CHANNEL_HEIGHT)
	var tint_map   = data.get_image(data.CHANNEL_COLOR)
	var splat_map  = data.get_image(data.CHANNEL_SPLAT)
	
	# Generate random maps
	var noise_seed = _noise.seed
	_noise.seed = randi()
	print("H seed ", _noise.seed)
	var hmap = _noise.get_seamless_image(resolution)
	_noise.seed = randi()
	var tmap = _noise.get_seamless_image(resolution)
	_noise.seed = randi()
	var smap = _noise.get_seamless_image(resolution)
	_noise.seed = noise_seed
	
	hmap.convert(height_map.get_format())
	tmap.convert(tint_map.get_format())
	smap.convert(splat_map.get_format())
	
	height_map.blit_rect(hmap, Rect2(0, 0, resolution, resolution), Vector2(resolution,resolution))
	data.notify_region_change(Rect2(0, 0, resolution, resolution), data.CHANNEL_HEIGHT)
	
	tint_map.blit_rect(tmap, Rect2(0, 0, resolution, resolution), Vector2(resolution,resolution))
	data.notify_region_change(Rect2(0, 0, resolution, resolution), data.CHANNEL_COLOR)
	
	splat_map.blit_rect(smap, Rect2(0, 0, resolution, resolution), Vector2(resolution,resolution))
	data.notify_region_change(Rect2(0, 0, resolution, resolution), data.CHANNEL_SPLAT)

