tool
extends Spatial

#=== Debug mode ==========
# Uncomment for export 
# Toggle comments: [STRG] + K
export(bool) var _clear setget setclear
func setclear(_val):
	clear()
export(bool) var _sample setget setsample
func setsample(_val):
	sample(0,0)
#========================

export(OpenSimplexNoise) var _noise
export(int) var _min_height = -2
export(int) var _max_height = 10

func _set_heightmap_from_image(image, max_height := null, min_height := null):
	var data = get_node("Terrain").get_data()
	var resolution = data.get_resolution()
	var path = data.get_data_dir() + "/height_texture.png"
	image.save_png(path)
	var hmax = max_height
	if hmax == null:
		hmax =  _max_height
	var hmin = min_height
	if hmin == null:
		hmin =  _min_height
	data._import_heightmap(path, hmin, hmax, false)
	data.notify_region_change(Rect2(0, 0, resolution, resolution), data.CHANNEL_HEIGHT)

func clear():
	var data = get_node("Terrain").get_data()
	var resolution = data.get_resolution()
	var format = data.get_channel_format(data.CHANNEL_HEIGHT)
	
	var image = Image.new()
	image.create(resolution, resolution, false, format)
	_set_heightmap_from_image(image, 0, 1)

func sample(i,j):
	var data = get_node("Terrain").get_data()
	var resolution = data.get_resolution()
	var format = data.get_channel_format(data.CHANNEL_HEIGHT)
	
	_noise.seed = randi()  
	#noise.seed = 1024 * i + j
	var image = _noise.get_seamless_image(resolution)
	_set_heightmap_from_image(image)

