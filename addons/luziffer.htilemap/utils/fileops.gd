# file system utilities

#extends Node

static func copy_folder(from, to, omit = []):
	
	# Assure arguments end with slash 
	if from[-1] != "/": from = from + "/"
	if to[-1] != "/": to = to + "/"
	
	# Open source folder
	var from_dir = Directory.new()
	from_dir.open(from)
	
	# Create directory if not exists
	var to_dir = Directory.new()
	if not to_dir.dir_exists(to):
		to_dir.make_dir(to)
	
	# Loop over sources
	from_dir.list_dir_begin()
	var file = from_dir.get_next()
	while file != "":
		if file != "." and file != ".." and not file in omit:
			
			# Copy directories by recursion
			if from_dir.current_is_dir():
				copy_folder(from + file, to + file)
			
			# Copy files directly
			else:
				from_dir.copy(from + file, to + file)
		file = from_dir.get_next() 


static func remove(path):
	var dir = Directory.new()
	
	# Recursively empty directory if necessary 
	if dir.dir_exists(path):
		dir.open(path)
		dir.list_dir_begin()
		var file = dir.get_next() 
		while file != "":
			if file != "." and file != "..":
				remove(path + "/" + file)
			file = dir.get_next()
	
	dir.remove(path)
