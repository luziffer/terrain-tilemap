extends Node2D

signal generated

#########################
# Internal
# Onready
onready var _drawer = $Renderer
onready var _shader_container = $Viewport/Shader
onready var _viewport = $Viewport

# ###
var _generated_image

func get_image() -> Image:
	if _generated_image != null:
		return _generated_image
	else:
		printerr("No image generated, use generate_image() and wait for \"generated\" signal")
		return null

func generate_image(material : Material, resolution := Vector2(512,512), multiplier := 1.0, args := {}):
	# Resize generating nodes
	_viewport.size = resolution
	_viewport.render_target_update_mode = Viewport.UPDATE_ALWAYS
	_shader_container.rect_size = resolution
	
	# Set material type
	_shader_container.set_material(material)
	
	# Set shaders param
	_shader_container.get_material().set_shader_param("resolution", resolution*multiplier)
	for arg in args:
		_shader_container.get_material().set_shader_param(arg, args[arg])
	
	## Actually Generate Image
	_drawer.show()
	yield(get_tree(),"idle_frame")
	yield(get_tree(),"idle_frame")
	yield(get_tree(),"idle_frame")
	_generated_image = _drawer.get_texture().get_data().duplicate()
	emit_signal("generated")
	_viewport.render_target_update_mode = Viewport.UPDATE_DISABLED
	_drawer.hide()
