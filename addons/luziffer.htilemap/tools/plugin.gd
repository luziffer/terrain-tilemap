tool #https://www.youtube.com/watch?v=MM62wjLrgmA
extends EditorPlugin


# Load classes
const HTilemap = preload("../HTilemap.gd")

var _navigator = null 
var _navigator_is_in_panel = false
var _tilemap = null

const COLOR_EXISTS = Color(1,.8,0)
const COLOR_EMPTY  = Color(0,.8,1)


func _enter_tree():
	
	# Register classes
	add_custom_type("HTilemap", "Spatial", HTilemap, preload("../icons/icon_htilemap_node.svg"))
	
	# Connect signals
	var selection = get_editor_interface().get_selection()
	selection.connect("selection_changed", self, "_selection_changed")
 
func _ready():
	_navigator = load("res://addons/luziffer.htilemap/ui/Navigator.tscn")
	_navigator = _navigator.instance()
	add_control_to_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_BOTTOM, _navigator)
	_navigator_is_in_panel = true
	_navigator.hide()
	_navigator.get_node("Index/tile_i").connect("value_changed", self, "_on_tile_i_value_changed")
	_navigator.get_node("Index/tile_j").connect("value_changed", self, "_on_tile_j_value_changed")
	_navigator.get_node("Edit/Tile").connect("pressed", self, "_on_edit_tile_pressed")
	_navigator.get_node("Data/Template").connect("pressed", self, "_on_edit_template_pressed")
	_navigator.get_node("Data/Clear").connect("pressed", self, "_on_clear_pressed")
	_navigator.get_node("Data/Sample").connect("pressed", self, "_on_sample_pressed")
	#_navigator.get_node("Data/Refresh").connect("pressed", self, "_on_refresh_pressed")


func _selection_changed():
	var worlds = []
	var selection = get_editor_interface().get_selection()
	for node in selection.get_selected_nodes():
		if node.get_class() == "HTilemap":
			worlds.append(node)
			
	# Interface only active if single open world tile selected
	if len(worlds) == 1:
		_tilemap = worlds[0]
		if _navigator != null:
			if not _navigator_is_in_panel:
				#add_control_to_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_BOTTOM, _navigator)
				add_control_to_bottom_panel(_navigator, "World Tilemap")
				_navigator_is_in_panel = true
			_navigator.get_node("Index/tile_i").value = _tilemap.tile_i
			_navigator.get_node("Index/tile_j").value = _tilemap.tile_j
			_navigator.show()
			_update()
	
	# Otherwise deactivate interface
	else:
		if _tilemap != null:
			_tilemap.preview_template()
			_tilemap = null
		if _navigator != null:
			if not _navigator_is_in_panel:
				#remove_control_from_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_BOTTOM, _navigator)
				remove_control_from_bottom_panel(_navigator)
				_navigator_is_in_panel = false
			_navigator.hide()
	
func _exit_tree():
	if _navigator_is_in_panel:
		#remove_control_from_container(EditorPlugin.CONTAINER_SPATIAL_EDITOR_BOTTOM, _navigator)
		remove_control_from_bottom_panel(_navigator)
		_navigator_is_in_panel = false
	_navigator.free()
	remove_custom_type("HTilemap")
 

func _update():
	if _tilemap != null:
			var i = _navigator.get_node("Index/tile_i").value
			var j = _navigator.get_node("Index/tile_j").value
			
			# Update status
			var status = _navigator.get_node("Data/Status")
			var edit_tile = _navigator.get_node("Edit/Tile")
			#var keep_tile = _navigator.get_node("Edit/Keep") 
			if _tilemap.tile_exists(i, j):
				status.set_text("exists")
				status.add_color_override("font_color", COLOR_EXISTS)
				edit_tile.disabled = false
				#keep_tile.disabled = false
			else: 
				status.set_text("empty")
				status.add_color_override("font_color", COLOR_EMPTY)
				edit_tile.disabled = true
				#keep_tile.disabled = true
			#keep.connect("toggled", )
			
			# Update view
			_tilemap.preview_tile(i,j)


# Callbacks 
func _on_tile_i_value_changed(value):
	_update()
func _on_tile_j_value_changed(value):
	_update()

func _on_edit_tile_pressed():
	var i = _navigator.get_node("Index/tile_i").value
	var j = _navigator.get_node("Index/tile_j").value
	if _tilemap.tile_exists(i,j):
		var id = String(i) + "_" + String(j)
		var path = _tilemap.tile_directory + "/" + id + "/Tile.tscn"
		get_editor_interface().open_scene_from_path(path)

func _on_edit_template_pressed():
	
	var path = _tilemap.template_directory + "/Tile.tscn"
	var dir = Directory.new()
	if dir.file_exists(path):
		get_editor_interface().open_scene_from_path(path)


func _on_clear_pressed():
	var i = _navigator.get_node("Index/tile_i").value
	var j = _navigator.get_node("Index/tile_j").value
	_tilemap._destroy_tile(i,j)
	_update()

func _on_sample_pressed():
	var i = _navigator.get_node("Index/tile_i").value
	var j = _navigator.get_node("Index/tile_j").value
	var tile = _tilemap._generate_tile(i,j)
	tile.queue_free()
	_update()

