tool
extends Spatial 
#class_name HTilemap

#why is this not automatically done?? 
#probably some GDScript weirdness... 
func get_class() -> String:
	return "HTilemap"


# Requires Heightmap Terrain Plugin
const HTerrain = preload("res://addons/zylann.hterrain/hterrain.gd")
const HTerrainData = preload("res://addons/zylann.hterrain/hterrain_data.gd")

const FileOps = preload("./utils/fileops.gd")

# Folders for in- and ouput data
export(String, DIR) var template_directory = ""
export(String, DIR) var tile_directory = ""

# Tiling
export(int) var tile_i : int = 0
export(int) var tile_j : int = 0
var tile_size : float = NAN # half of actual tile size

# Movement
export(NodePath) var trigger_node = "" setget set_trigger
export(float, 0.5, 1.4) var transition_tolerance = 1.0
var _trigger : Spatial = null 
signal moved(vector)


# Debugging
func _get_configuration_warning() -> String:
	var dir = Directory.new()
	
	# Check tile directory
	if tile_directory == "":
		return "Don't forget to select the 'Tile Directory' property."
	else:
		if not dir.dir_exists(tile_directory):
			return "Directory '"+tile_directory+"' does not exist."
	
	# Check template 
	if not dir.dir_exists(template_directory):
		return "Directory '"+template_directory+"' does not exist."
	elif not dir.file_exists(template_directory + "/Tile.tscn"):
		return "Template folder must contain a scene named 'Tile.tscn'"
	else:
		var tile = load(template_directory + "/Tile.tscn").instance()
		if not tile is Spatial:
			return "Tile node has class " + tile.get_class() + " instead of HTile!"
		if not tile.has_node("Terrain"):
			return "Tile scene must contain a heightmap terrain named 'Terrain'"
		var terrain = tile.get_node("Terrain")
		if terrain.map_scale.x != terrain.map_scale.z:
			return "Template terrain must be quadratic!" 
	
	# Check movement trigger
	if trigger_node == "":
		print("Don't forget to set a trigger node!")
	#elif not _trigger is Spatial:
	#	print("Trigger node must inherit from Spatial. It is ", _trigger.get_class())
	
	return ""

func _enter_tree():
	print("Enter ", name)
	
func _exit_tree():
	print("Exit ", name)

# Display
func preview_template():
	var dir = Directory.new() 
	var file = template_directory + "/Tile.tscn"
	if dir.file_exists(file):
		_preview(load(file).instance())
	else:
		_preview(Spatial.new())

func preview_tile(i,j):
	if not tile_exists(i,j):
		preview_template()
	else:
		var id = String(i) + "_" + String(j)
		_preview(load(tile_directory + "/" + id + "/Tile.tscn").instance())

func _preview(tile):
	
	# Clear previous preview
	for c in get_children():
		c.queue_free()
	
	# Adopt new tile
	tile.name = "preview"
	tile.translation.x = 0
	tile.translation.z = 0 
	add_child(tile)

func _ready():
	set_process(false)
	_set_template(template_directory)
	if not Engine.editor_hint:
		set_process(true)
		for dx in [-1,0,1]:
			for dy in [-1,0,1]:
				_adjust_tile(tile_i + dx, tile_j + dy)
		set_trigger(trigger_node)
	else:
		preview_template()


func _set_template(template_folder):
	template_directory = template_folder
	var tile = load(template_directory + "/Tile.tscn").instance()
	var terrain = tile.get_node("Terrain")
	var resolution = terrain.get_data()._resolution - 1
	var scale = terrain.map_scale[0]
	tile_size = scale * resolution 

#	# Center template terrain
#	var p = terrain.translation
#	if p.x != -tile_size / 2 or p.z != -tile_size / 2:
#		terrain.translation.x = -tile_size / 2
#		terrain.translation.z = -tile_size / 2
#		var scene = PackedScene.new()
#		scene.pack(tile)
#		ResourceSaver.save(template_directory + "/Tile.tscn", scene) 

func tile_exists(i,j) -> bool:
	var d = Directory.new()
	var id = String(i) + "_" + String(j)
	if d.dir_exists(tile_directory):
		if d.file_exists(tile_directory + "/" + id + "/Tile.tscn"):
			return true
	return false

func _create_tile(i,j):
	var id = String(i) + "_" + String(j)
	var path = tile_directory + "/" + id
	if tile_exists(i,j):
		FileOps.remove(path)
	FileOps.copy_folder(template_directory, path, ["share"])
	
	# Edit resource manually
	var file = File.new()
	file.open(path + "/Tile.tscn", File.READ_WRITE)
	var contents = file.get_as_text()
	contents = contents.replace(template_directory+"/", path+"/")
	contents = contents.replace(path+"/share", template_directory+"/share")
	file.store_string(contents)
	file.close()
	
	# Load and rename scene 
	var scene = load(path + "/Tile.tscn").instance()
	scene.name = id
	return scene 

func _load_tile(i,j):
	var id = String(i) + "_" + String(j)
	print("Loading tile ", id)

#	# Check if node is already present 
#	if has_node(id):
#		return get_node(id) 
	
	# Create node
	var tile = null
	if not tile_exists(i,j):
		tile = _generate_tile(i,j)
	else:
		tile = load(tile_directory + "/" + id + "/Tile.tscn" ).instance()
	
	# Adopt node as child
	tile.name = id
	add_child(tile) 
	return tile

func _destroy_tile(i,j):
	var id = String(i) + "_" + String(j)
	print("Destroying tile ", id)
	FileOps.remove(tile_directory + "/" + id)

func _close_tile(i,j):
	var id = String(i) + "_" + String(j)
	print("Closing tile ", id)
	if not has_node(id):
		print("Child ", id, " not found...")
		for c in get_children():
			print(" - ", c.name, " : ", c)
	var tile = get_node(id)
	tile.translation = Vector3()
	tile.name = "Tile"
	var scene = PackedScene.new()
	scene.pack(tile)
	var status = ResourceSaver.save(tile_directory + "/" + id + "/Tile.tscn", scene)
	if status != OK:
		print("Failed to save tile ", [i, j],  "!")
	tile.queue_free()


func set_trigger(node_path):
	trigger_node = node_path
	if has_node(trigger_node):
		_trigger = get_node(trigger_node)

#	if node extends Spatial:
#		trigger = node
#	else: 
#		print("Type ", node.get_class(), " does not extend Spatial!")
#		trigger = null

func move(dx, dy):
	print("Moving ", dx, " ", dy)
	var affected_indices = []
	for di in [-1,0,1]:
		for dj in [-1,0,1]:
			affected_indices.append([tile_i+di, tile_j+dj])
	tile_i += dx
	tile_j += dy
	for di in [-1,0,1]:
		for dj in [-1,0,1]:
			var i = tile_i+di
			var j = tile_j+dj
			if not [i,j] in affected_indices:
				affected_indices.append([tile_i+di, tile_j+dj])
	print("Affected indices ", affected_indices)
	
	# Update tiles and trigger
	for index in affected_indices:
		_adjust_tile(index[0], index[1])
#	var threads = []
#	for index in affected_indices:
#		var thread = Thread.new()
#		thread.start(self, "_adjust_tile", index[0], index[1]) 
#		threads.append(thread)
	var trafo = _trigger.get_global_transform()
	trafo.origin.x -= dx * tile_size
	trafo.origin.z += dy * tile_size
	_trigger.set_global_transform(trafo)

	emit_signal("moved", Vector3(dx*tile_size, 0, -dy*tile_size))
#	for thread in threads:
#		thread.wait_to_finish()

func _adjust_tile(i,j):
	var id = String(i) + "_" + String(j)
	var dx = i - tile_i
	var dy = j - tile_j 
	if abs(dx) >= 2 or abs(dy) >= 2:
		_close_tile(i,j)
	else:
		var tile
		if has_node(id):
			tile = get_node(id)
		else:
			tile = _load_tile(i,j)
		
		var trafo = tile.get_global_transform()
		trafo.origin.x =  dx * tile_size 
		trafo.origin.z = -dy * tile_size 
		tile.set_global_transform(trafo)

func _process(delta):
	var p = _trigger.get_global_transform().origin
	p -= get_global_transform().origin
	var rel_x =  p.x / tile_size
	var rel_y = -p.z / tile_size
	
	if abs(rel_x) > transition_tolerance \
	or abs(rel_y) > transition_tolerance:
		rel_x = round(rel_x)
		rel_y = round(rel_y)
		if rel_x != 0 or rel_y != 0:
			move(rel_x, rel_y)
	
	#var remaining_x = (tile_size * (.5+transition_tolerance)) - abs(p.x)
	#var remaining_y =  (tile_size * (.5+transition_tolerance)) - abs(p.z)
	#if remaining_x < 100 or remaining_y < 100:
	#	print("Remaining %.3f %.3f " % [remaining_x, remaining_y])
	# 
	#if abs(p.x) > tile_size * (.5+transition_tolerance) \
	#or abs(p.z) > tile_size * (.5+transition_tolerance):
	#	var dx = sign(  p.x / tile_size ) 
	#	var dy = sign( -p.z / tile_size )
	#	if not (dx == 0 and dy == 0):
	#		move(dx, dy)

func _generate_tile(i,j):
	print("Generating tile ", [i, j])
	if tile_exists(i,j):
		_destroy_tile(i,j) 
	var tile = _create_tile(i,j)
	
	if not tile.has_method("sample"):
		print("Sample method not found!")
	else:
		tile.sample(i,j)
	return tile
